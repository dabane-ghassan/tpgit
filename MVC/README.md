# GUI
- To launch the GUI:
```bash
python3 main_gui.py
```

# CLI
- To get information about the CLI:
```bash
python3 main_cli.py -h
```
- To search for a person in the phone book:
```bash
python3 main_cli.py --search firstname
```
- To insert a person, please provide all the information:
```bash
python3 main_cli.py --insert firstname,lastname,phone,address,city
```

