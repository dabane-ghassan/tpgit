#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Application's controller class.

@author: Ghassan Dabane
"""
from __future__ import absolute_import
from typing import Dict, List
from model import Individual, PhoneBook

class Controller:
    """A class to implement a controller.

    Attributes
    ----------
    database: PhoneBook
        A phonebook class instance.
    working_dict: Dict[str, Individual]
        A dictionary to represent the working memory of the program.

    """
    def __init__(self: object, file_path: str) -> None:
        """The class constructor

        Parameters
        ----------
        file_path : str
            File path for the .tsv file.

        Returns
        -------
        None
            A class instance.

        """
        self.database = PhoneBook(file_path)
        self.working_dict = self.load_phonebook()

    def load_phonebook(self: object) -> Dict[str, Individual]:
        """This method loads the .tsv file into a dictionary.

        Returns
        -------
        Dict[str, Individual]
            The working memory of the file.

        """
        return self.database.book_to_dict()

    def insert_to_phonebook(self: object, **kwargs: Dict[str, str]) -> None:
        """This method inserts an indivdual into the database.

        Parameters
        ----------
        **kwargs : Dict[str, str]

        Returns
        -------
        None

        """
        new = Individual(fname = kwargs["first_name"],
                         lname = kwargs["last_name"],
                         phone = kwargs["phone"],
                         address = kwargs["address"],
                         city = kwargs["city"])

        self.database.insert_to_book(new)

    def find_individual(self: object, fname: str) -> str:
        """This method finds an individual in the working memory.

        Parameters
        ----------
        fname : str
            The first name of the person to be searched.

        Returns
        -------
        str
            Information of the person or a "didn't find message".

        """
        for name in self.working_dict:
            if name.lower() == fname.lower():
                return self.working_dict[name].tabulated_repr()

        return "This Person is not available"
    
    def all_fnames(self: object) -> List[str]:
        """Returns all first names that are present in the PhoneBook.

        Returns
        -------
        List[str]
            All first names.

        """
        return list(self.working_dict.keys())
