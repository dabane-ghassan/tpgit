#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A script to create phonebook with one entry (a database-like file .tsv)

@author: Ghassan Dabane
"""
from __future__ import absolute_import
from model import Individual, PhoneBook

if __name__ == "__main__":

    NEW_BOOK = PhoneBook.create_new('myphonebook.tsv')
    me = Individual(fname = "Ghassan", lname = "Dabane",
                    phone = "0750699460", address = "104 Bd de Paris",
                    city = "Marseille")
    curr_book = PhoneBook(NEW_BOOK)
    curr_book.insert_to_book(me)
