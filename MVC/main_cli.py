"""
A CLI to replace the GUI for the PhoneBook.

@author: Ghassan Dabane.
"""
from __future__ import absolute_import
import argparse
from controller import Controller

CONT = Controller('myphonebook.tsv') # A controller instance

def search(person: str) -> None:
    """Searches for a contact in the controller.

    Parameters
    ----------
    person : str
        A person's first name.

    Returns
    -------
    None

    """
    print(CONT.find_individual(person))

def insert(person: str) -> None:
    """Inserts a contact into The Phone Book.

    Parameters
    ----------
    person : str
        Contact information to be inserted, a list of information separated
        by a comma; firstname,lastname,phone,address,city.

    Returns
    -------
    None

    """
    infos = person.split(',')
    CONT.insert_to_phonebook(first_name = infos[0], last_name = infos[1],
                                  phone = infos[2], address = infos[3],
                                  city = infos[4])
    CONT.working_dict = CONT.load_phonebook()
    print("The contact has been inserted.")

def create_parser() -> argparse.ArgumentParser:
    """This function creates the parser for our CLI."""

    parser = argparse.ArgumentParser(add_help=True,
                                     description="""A Phone Book CLI, search
                                     for and insert contacts""")
    parser.add_argument('-i','--insert',
                        type=str,
                        help="""Contact information seperated with a comma.
                        Please provide all following information.
                        firstname,lastname,phone,address,city""")
    parser.add_argument('-s','--search',
                        type=str,
                        help="A person's first name.")
    return parser

def main() -> None:
    """This function instantiates an argument parser object and calls
    selec_seqs_list function to return sequences for user specified IDs
    from the given fasta file.
    """
    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__
    if args['search']:
        search(args['search'])
    if args['insert']:
        insert(args['insert'])

if __name__ == "__main__":
    main()
