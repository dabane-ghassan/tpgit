#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A script to launch the GUI of the app.

@author: Ghassan Dabane
"""
from __future__ import absolute_import
from tkinter import Tk
from view_gui import App

if __name__ == "__main__":
    root = Tk()
    App(root)
    root.mainloop()
