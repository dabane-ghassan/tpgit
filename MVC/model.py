#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Application's data model class.

@author: Ghassan Dabane
"""
from __future__ import absolute_import
from typing import Dict

class Individual:
    """A class to represent an individual.

    Attributes
    ----------
    fname: str
        The individual's first name.
    lname: str
        The individual's last name.
    phone: str
        The individual's phone number.
    address: str
        The individual's address.
    city: str
        The individual's city.

    """
    def __init__(self: object, **kwargs: Dict[str, str]) -> None:
        """The class constructor.

        Parameters
        ----------
        **kwargs : Dict[str, str]
            Individual's information.

        Returns
        -------
        None
            A class instance.

        """

        self.fname = kwargs['fname']
        self.lname = kwargs['lname']
        self.phone = kwargs['phone']
        self.address = kwargs['address']
        self.city = kwargs['city']

    def __str__(self: object) -> str:
        """Returns a string representation using print().

        Returns
        -------
        str
            A string representation of an individual.

        """
        return "%s %s" %(self.fname, self.lname)

    def tabulated_repr(self: object) -> str:
        """Returns a tabulated string representation of all properties.

        Returns
        -------
        str
            All individual's information.

        """
        return '\t'.join(list(self.__dict__.values()))

class PhoneBook:
    """A class to represent a phone book.

    Attributes
    ----------
    path: str
        The file path for the phonebook .tsv file.

    """
    def __init__(self: object, path: str) -> None:
        """The class constructor.

        Parameters
        ----------
        path : str
            The file path.

        Returns
        -------
        None
        """
        self.path = path

    @staticmethod
    def create_new(name: str) -> str:
        """This method creates a new .tsv file for the phonebook.

        Parameters
        ----------
        name : str
            file's name and path.

        Returns
        -------
        str
            file name and path.
        """

        fields = ["first_name", "last_name", "phone", "address", "city"]
        with open(name, 'w') as phb:
            phb.write('\t'.join(fields))

        return name

    def book_to_dict(self: object) -> Dict[str, Individual]:
        """This method transforms a .tsv phonebook file into a dictionary.

        Returns
        -------
        Dict[str, Individual]
            A dictionary of first name and Individual objects.

        """
        data = {}
        with open(self.path, 'r') as phb:
            lines = phb.readlines()[1:] # not taking the header
            for line in lines:
                line = line.split('\t')
                data[line[0]] = Individual(fname = line[0], lname = line[1],
                                           phone = line[2], address = line[3],
                                           city =  line[4])
        return data

    def insert_to_book(self: object, individual: Individual) -> None:
        """This method inserts an Individual object into a .tsv file.

        Parameters
        ----------
        individual : Individual
            The person to be inserted.

        Returns
        -------
        None

        """

        with open(self.path, 'a') as phb:
            phb.write('\n' + individual.tabulated_repr())

    def dict_to_book(self: object, data_dict: Dict[str, Individual]) -> None:
        """This method inserts from a dictionary to a .tsv file.

        Parameters
        ----------
        data_dict : Dict[str, Individual]
            The working memory dictionary.

        Returns
        -------
        None

        """

        for ind in data_dict.values():
            self.insert_to_book(ind)
