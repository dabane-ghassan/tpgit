#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Application's view class (A Tkinter GUI).

@author: Ghassan Dabane
"""
from __future__ import absolute_import
import re
from tkinter import Entry, Label, StringVar, Button, Frame, messagebox, Tk, Listbox, END, ACTIVE
from controller import Controller

CONT = Controller('myphonebook.tsv') # A Controller instance

class AutocompleteEntry(Entry):

    def __init__(self, autocompleteList, *args, **kwargs):

        # Listbox length
        if 'listboxLength' in kwargs:
            self.listboxLength = kwargs['listboxLength']
            del kwargs['listboxLength']
        else:
            self.listboxLength = 8

        # Custom matches function
        if 'matchesFunction' in kwargs:
            self.matchesFunction = kwargs['matchesFunction']
            del kwargs['matchesFunction']
        else:
            def matches(fieldValue, acListEntry):
                pattern = re.compile('.*' + re.escape(fieldValue) + '.*', re.IGNORECASE)
                return re.match(pattern, acListEntry)
                
            self.matchesFunction = matches

        Entry.__init__(self, *args, **kwargs)
        self.focus()

        self.autocompleteList = autocompleteList
        
        self.var = self["textvariable"]
        if self.var == '':
            self.var = self["textvariable"] = StringVar()

        self.var.trace('w', self.changed)
        self.bind("<Right>", self.selection)
        self.bind("<Up>", self.moveUp)
        self.bind("<Down>", self.moveDown)
        
        self.listboxUp = False

    def changed(self, name, index, mode):
        if self.var.get() == '':
            if self.listboxUp:
                self.listbox.destroy()
                self.listboxUp = False
        else:
            words = self.comparison()
            if words:
                if not self.listboxUp:
                    self.listbox = Listbox(width=self["width"], height=self.listboxLength)
                    self.listbox.bind("<Button-1>", self.selection)
                    self.listbox.bind("<Right>", self.selection)
                    self.listbox.place(x=self.winfo_x(), y=self.winfo_y() + self.winfo_height())
                    self.listboxUp = True
                
                self.listbox.delete(0, END)
                for w in words:
                    self.listbox.insert(END,w)
            else:
                if self.listboxUp:
                    self.listbox.destroy()
                    self.listboxUp = False
        
    def selection(self, event):
        if self.listboxUp:
            self.var.set(self.listbox.get(ACTIVE))
            self.listbox.destroy()
            self.listboxUp = False
            self.icursor(END)

    def moveUp(self, event):
        if self.listboxUp:
            if self.listbox.curselection() == ():
                index = '0'
            else:
                index = self.listbox.curselection()[0]
                
            if index != '0':                
                self.listbox.selection_clear(first=index)
                index = str(int(index) - 1)
                
                self.listbox.see(index) # Scroll!
                self.listbox.selection_set(first=index)
                self.listbox.activate(index)

    def moveDown(self, event):
        if self.listboxUp:
            if self.listbox.curselection() == ():
                index = '0'
            else:
                index = self.listbox.curselection()[0]
                
            if index != END:                        
                self.listbox.selection_clear(first=index)
                index = str(int(index) + 1)
                
                self.listbox.see(index) # Scroll!
                self.listbox.selection_set(first=index)
                self.listbox.activate(index) 

    def comparison(self):
        return [w for w in self.autocompleteList if self.matchesFunction(self.var.get(), w)]

class App(Frame):
    """The app's GUI.

    Attributes
    ----------
    widgets_labs: Dict[str, tkinter.Label]
        A dictionary of labels.
    widgets_entry: Dict[str, tkinter.Entry]
        A dictionary of entries.
    widgets_button: Dict[str, tkinter.Button]
        A dictionary of buttons.

    """
    def __init__(self: object, master: Tk) -> None:
        """Class constructor.

        Parameters
        ----------
        master : Tk
            The root window, Tk() instance.

        Returns
        -------
        None

        """
        Frame.__init__(self, master, width=45, height=45)
        master.title("Phone Book")

        msg = "Hint: Enter a first name to search for a contact"
        messagebox.showinfo("Welcome", msg)

        ids = ["first name", "last name", "phone", "address", "city", "search"]
        buttons = ["Search", "Insert", "Clear"]

        self.widgets_labs = {}
        self.widgets_entry = {}
        self.widgets_button = {}

        i, j = 0, 0

        for idi in ids:
               
            lab = Label(master, text=idi)
            self.widgets_labs[idi] = lab
            lab.grid(row=i,column=0)

            var = StringVar()
            if idi == "first name":
                entry = AutocompleteEntry(CONT.all_fnames(), master)
            else:
                entry = Entry(master, text=var)
            self.widgets_entry[idi] = entry
            entry.grid(row=i,column=1)
            i += 1

        for btn in buttons:
            button = Button(master, text = btn)
            self.widgets_button[btn] = button
            button.grid(row=i+1,column=j)
            j += 1

        self.widgets_button["Insert"].config(command = self.insert_cmd)
        self.widgets_button["Search"].config(command = self.search_cmd)
        self.widgets_button["Clear"].config(command = self.clear_cmd)

    def clear_cmd(self: object) -> None:
        """The clear command to go with the button.

        Returns
        -------
        None

        """
        for entry in self.widgets_entry.values():
            entry.delete(0, 'end')

    def search_cmd(self: object) -> None:
        """The search command to go with the button.

        Returns
        -------
        None

        """
        inp = self.widgets_entry["first name"].get()

        if not inp:
            err = "Please enter a first name"
            messagebox.showerror("No information found", err)

        else:
            messagebox.showinfo("Search results", CONT.find_individual(inp))

    def insert_cmd(self):
        """The insert command to go with the button.

        Returns
        -------
        None

        """
        fname = self.widgets_entry["first name"].get()
        lname = self.widgets_entry["last name"].get()
        phone = self.widgets_entry["phone"].get()
        address = self.widgets_entry["address"].get()
        city = self.widgets_entry["city"].get()

        if fname and lname and phone:
            CONT.insert_to_phonebook(first_name = fname, last_name = lname,
                                  phone = phone, address = address,
                                  city = city)
            CONT.working_dict = CONT.load_phonebook()
            self.widgets_entry['first name'].autocompleteList = CONT.all_fnames()
            messagebox.showinfo("Success", "The contact has been inserted")

        else:
            err = "Please enter atleast first name, last name and phone number"
            messagebox.showerror("Full info required", err)
