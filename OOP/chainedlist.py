# coding: utf-8
"""A chained list class to represent a linked list from a bunch of Node
objects - OOP practical work - INLO.

@author : Ghassan DABANE
"""
from __future__ import absolute_import
from typing import List
from node import Node

class ChainedList:
    """A class to simulate the behavior of linked lists, will build a linked
    list object from a python list of integer values.

    Attributes
    ----------
    first_node : node.Node
        A node object.

    """

    def __init__(self: object, nodes: List[int]) -> None:
        """Class constructor.

        Parameters
        ----------
        nodes : List[int]
            list that we want to transfert in a chained list of Node object.

        Returns
        -------
        None
            Chained list object.

        """
        self.first_node = Node(nodes[0])
        curr_node = self.first_node
        for val in nodes[1:]:
            curr_node.link = Node(val)
            curr_node = curr_node.link

    def __str__(self: object) -> str:
        """This method returns a string representation of the object.

        Returns
        -------
        str
            A representation of the linked list.

        """
        return str(self.first_node)

    def __repr__(self:object) -> str:
        """This method returns a coder friendly representation of a class
        instance.

        Returns
        -------
        str

        """
        return "ChainedList(%s)" % (str(self.first_node))

    def insert_node_after(self: object, data: int, new_node: Node) -> None:
        """This function inserts a new node after the node with the
        value == data.

        Parameters
        ----------
        data : int
            the value of node for which we want to insert the new node after.
        new_node : Node
            The new node to be inserted.

        Returns
        -------
        None
            Inserts the new node after the chosen position in the linked list.

        """
        curr_node = self.first_node #loop to find the index of the node
        while curr_node is not None:
            if curr_node.data == data:
                break
            curr_node = curr_node.link

        new_node.link = curr_node.link #insertion between two nodes
        curr_node.link = new_node

    def delete_node(self, data: int) -> None:
        """This method deletes all node(s) where value == data.

        Parameters
        ----------
        data : int
            the value of the node to be deleted.

        Returns
        -------
        None

        """
        if self.first_node.data == data: #check for the first node before loop
            self.first_node = self.first_node.link

        curr_node = self.first_node #loop to find the node of interest
        while curr_node.link is not None:
            if curr_node.link.data == data:
                curr_node.link = curr_node.link.link #deletion
            curr_node = curr_node.link

    def get_list(self: object) -> List[int]:
        """This method returns a Python list object from a linked list object.

        Returns
        -------
        List[int]
            A Python list.

        """
        data = []
        curr_node = self.first_node
        while curr_node is not None:
            data.append(curr_node.data)
            curr_node = curr_node.link
        return data

    def binary_search(self, element: int, start: int, end: int) -> int:
        """This method implements binary search on the linked list.

        Parameters
        ----------
        element : int
            The element to be searched.
        start : int
            the starting index.
        end : int
            the ending index.

        Returns
        -------
        int
            The index of the element to be searched.

        """
        arr = self.get_list()
        mid = (start + end) // 2
        if arr[mid] == element:
            return mid
        else:
            if arr[mid] > element:
                return self.binary_search(element, start, mid-1)
            else:
                return self.binary_search(element, mid+1, end)
