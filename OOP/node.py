# coding: utf-8
"""A node class for the linked list - OOP practical work - INLO.

@author : Ghassan DABANE
"""

class Node:
    """A class that represents nodes in a linked list, each node can hold data
    and a pointer to the next data inside the list (link). for simplicity
    we're going to represent only a linked list of nodes with integer values.

    Attributes
    ----------
    param_data : int
        the data at a certain location in a list.

    link : node.Node
        another Node object.

    """

    def __init__(self: object, param_data: int) -> None:
        """Class constructor.

        Parameters
        ----------
        param_data : int
            the data to be held in a specific case in this node.

        Returns
        -------
        None
            Node object.

        """

        self.__data = param_data
        self.__link = None

    @property
    def data(self: object) -> int:
        """data property getter.

        Returns
        -------
        int
            the value of the data in the node.
        """
        return self.__data

    @data.setter
    def data(self: object, param_data: int) -> None:
        """data property setter.

        Parameters
        ----------
        param_data : int
            the value of the data.

        Returns
        -------
        None
        """
        self.__data = param_data

    @property
    def link(self: object):
        """link property getter.

        Returns
        -------
        node.Node
            the next node.
        """
        return self.__link

    @link.setter
    def link(self: object, new_node) -> None:
        """link property setter.

        Parameters
        ----------
        new_node : node.Node
            The link to the next node.

        Returns
        -------
        None

        """
        self.__link = new_node

    def __str__(self: object) -> str:
        """Returns a string representation of each node and its connection.

        Returns
        -------
        str
            a string representation of each node and its connection.

        """

        affichage = []
        node = self

        while node is not None:
            affichage.append(str(node.data))
            node = node.link

        return '->'.join(affichage)

    def __repr__(self: object) -> str:
        """A coder friendly representation of the object Node().

        Returns
        -------
        str

        """
        return "Node(%s)" % (self.data)
