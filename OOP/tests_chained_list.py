# coding: utf-8
"""A script to test the Node And Chained list classes - OOP practical work -
INLO.

@author : Ghassan DABANE
"""
from __future__ import absolute_import
from node import Node
from chainedlist import ChainedList

def test_separator(func):
    """this function is used to wrap individual tests."""
    def wrapper():
        print("==============================================")
        func()
        print("==============================================")
    return wrapper

@test_separator
def test_print_node() -> None:
    """This function tests the print functionality of the Node Class."""
    print("starting test print node")
    print("creating a bunch of nodes and linking them...")
    n_one = Node(1)
    n_two = Node(5)
    n_three = Node(90)
    n_four = Node(200)
    n_five = Node(4)
    n_one.link = n_two
    n_two.link = n_three
    n_three.link = n_four
    n_four.link = n_five
    print("now let's print the first one to see what happens...")
    print(n_one)

@test_separator
def test_insert_chainedlist() -> None:
    """This function tests the insert method of the ChainedList class."""
    print("starting test insert chainedlist")
    print("creating a linked list...")
    chained_list = ChainedList([1,5,6,12,34])
    print("this is a linked list: ")
    print(chained_list)
    print("let's insert the node 900 after the node 6...")
    chained_list.insert_node_after(6, Node(900))
    print("Here's the full list after insertion: ")
    print(chained_list)

@test_separator
def test_delete_chainedlist() -> None:
    """This function tests the delete method of the ChainedList class."""
    print("starting test delete chainedlist")
    print("creating a linked list...")
    chained_list = ChainedList([1,5,6,12,34,54,90,25])
    print("this is a linked list: ")
    print(chained_list)
    print("let's insert the node 1 in some random positions...")
    chained_list.insert_node_after(6, Node(1))
    chained_list.insert_node_after(54, Node(1))
    chained_list.insert_node_after(90, Node(1))
    print("Here's the full list after insertion: ")
    print(chained_list)
    print("Now we want to delete every node that has the value 1")
    print("Deleting...")
    chained_list.delete_node(1)
    print("Here's the full list after deletion: ")
    print(chained_list)

@test_separator
def test_binary_search() -> None:
    """This function tests the binary search method of the ChainedList
    class."""
    print("starting test binary search")
    print("creating a linked list...")
    chained_list = ChainedList([1,5,6,12,34,60,54,90,25])
    print("this is a linked list: ")
    print(chained_list)
    print("let's search for the node 90 in this list...")
    pos = chained_list.binary_search(90, 0, len(chained_list.get_list()) - 1)
    print("the index is: %s" % (pos))
    print("As we can see it successfully found it.")

if __name__ == "__main__":

    test_print_node()
    test_insert_chainedlist()
    test_delete_chainedlist()
    test_binary_search()
    chained_list = ChainedList([1,5,6,12,34,54,90,25])
    chained_list.delete_node(25)