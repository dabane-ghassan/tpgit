# -*- coding: utf-8 -*-
"""
TP OOP - Points du plan

Script pour le class LignePol

@author: Ghassan DABANE
"""
from typing import List, Tuple
from point import Point

class LignePol:
    """Ligne polygonale définie par un certain nombre de points,ses sommets."""

    def __init__(self: object, sommets: List[Tuple[int, int]]) -> None:
        self.__sommets = [Point(*coords) for coords in sommets]
        self.__nb_sommets = len(self.__sommets)

    def __str__(self: object) -> str:     
        return "%s" % self.__sommets

    def __repr__(self: object) -> str:
        return "LignePol(%d)" % self.__nb_sommets

    def get_sommet(self: object, i: int) -> Point:
        return self.__sommets[i]

    def set_sommet(self: object, i: int, p: Point) -> None:
        self.__sommets[i] = p

    def homothetie(self: object, k: float) -> None:
        for s in self.__sommets:
            s.homothetie(k)

    def translation(self: object, dx: float, dy: float) -> None:
        for s in self.__sommets:
            s.translation(dx, dy)

    def rotation(self: object, a: float) -> None:
        for s in self.__sommets:
            s.rotation(a)

    def tracer(self: object) -> None:
        pass
