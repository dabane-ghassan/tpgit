class Pile:
    """Implementation des piles avec des listes python."""
    
    def __init__(self: object, elements: list =[]) -> None:

        self.pile = elements
       
    def estVide(self: object) -> bool:
        
        return self.pile == []
    
    def empiler(self: object, e: int) -> None:
        
        self.pile.append(e)
    
    def depiler(self: object) -> list:
        
        if self.estVide():
            raise IndexError("attempt to pop from empty stack")
        return self.pile.pop()
    
    def sommet(self: object) -> int:
        
        if self.estVide():
            raise IndexError("attempt to read top of empty stack")
        return self.pile[-1]


class File:
    """Implementation des files avec des listes python."""
    
    def __init__(self: object, elements: list =[]) -> None:
        self.file = elements
    
    def estVide(self: object) -> bool:
        return self.file == []
    
    def enfiler(self: object, e: int) -> None:
        self.file.append(e)
    
    def defiler(self: object) -> list:
        if self.estVide():
            raise IndexError("attempt to remove element from empty queue")
        return self.file.pop(0)
    
    def tete(self: object) -> int:
        if self.estVide():
            raise IndexError("attempt to read head of empty queue")
        return self.file[0]


if __name__ == "__main__": # ne pas toucher au code ci-dessous
    f = File()
    for i in range(5):
        f.enfiler(i)
    while not f.estVide():
        print(f.defiler())
    try:
        f.defiler()
    except IndexError as e:
        print(e)
    try:
        print(f.tete())
    except IndexError as e:
        print(e)

    p = Pile()
    for i in range(5):
        p.empiler(i)
    while not p.estVide():
        print(p.depiler())
    try:
        p.depiler()
    except IndexError as e:
        print(e)
    try:
        print(p.sommet())
    except IndexError as e:
        print(e)
