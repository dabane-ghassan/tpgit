# -*- coding: utf-8 -*-
"""
TP OOP - Points du plan

Script pour le class Point

@author: Ghassan DABANE
"""
import math

class Point:
    """Un point dans l'espace bi-dimensionnelle."""

    def __init__(self: object, x: float, y: float) -> None:
        self.__x, self.__y = x, y

    @property
    def x(self: object) -> float:
        return self.__x

    @x.setter
    def x(self: object, x: float) -> None:
        self.__x = x

    @property
    def y(self: object) -> float:
        return self.__y

    @y.setter
    def y(self: object, y: float) -> None:
        self.__y = y

    def __str__(self: object) -> str:
        return "(%.1f, %.1f)" % (self.x, self.y)

    def __repr__(self: object) -> str:
        return "Point(%.1f, %.1f)" % (self.x, self.y)

    def __eq__(self: object, autrePoint) -> bool:
        return (isinstance(autrePoint, Point) and 
                self.x == autrePoint.x and
                self.y == autrePoint.y)      

    def r(self: object) -> float:
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def t(self: object) -> float:
        return math.atan2(self.y, self.x)

    def homothetie(self: object, k: float) -> None:
        self.x, self.y = k * self.x, k * self.y

    def translation(self: object, dx: float, dy: float) -> None:
        self.x, self.y = self.x + dx, self.y + dy

    def rotation(self: object, a: float) -> None:
        angle = self.t() + a
        rayon = self.r()
        self.x, self.y = rayon * math.cos(angle), rayon * math.sin(angle)
