# INLO M1 Bioinformatique

## TP - Tests
In order to test the class LinkedList please run the following command : 
```bash
python3 -m unittest test_linkedlist.py -v
```

## TP Gitlab 
TP d'introduction à l'utilisation de git : faire des commits, création des branches
séparés pour la doc et le développement du code, initialisation des répertoires, ......

## TP readability - argparse
Please install Biopython before using select_fasta CLI, example of use : 
```bash
python3 select_fasta.py -i some_seq_from_ensembl.fa -seq ENST00000377836,ENST00000476803,ENST00000496905
```

## TP - OOP
Please run the tests script to see the Node and ChainedList classes in action : 

```bash
python3 tests_chained_list.py
```

