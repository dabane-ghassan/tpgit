"""
Some tests, monday, 1 February.

@author: Ghassan Dabane

P.S: potential "Dark" spoilers ahead.
"""
from __future__ import absolute_import
from tree import TreeNode, Tree

if __name__ == "__main__":
    FAMILY = "((Katharina;Regina):Hannah;(Charlotte;Bartosz):Ulrich):Martha;((Tronte;Franziska):Magnus;(Claudia;Hanno):Mikkel):Jonas):Origin"
    tree = Tree.tree_from_nwk(FAMILY) #only a full tree
    tree.level_traversal() #print a level order traversal of a tree

    print(tree.height) #height from 0
    print(tree.print_newicklike()) #verifying that the tree has been fully integrated
    print(tree.count_leafs(tree.root)) #counting leafs
    print(tree.count_full_nodes()) #counting nodes with two children
    print(tree.count_all_nodes(tree.root)) # all nodes

    n1, n2, n3 = TreeNode('Helge'), TreeNode('Peter'), TreeNode('Elisabeth')
    tree.add_node(n1, 'Origin') #insert a new node after the root
    tree.level_traversal()

    tree.add_node(n2, 'Helge') #insert in the middle
    tree.level_traversal()

    tree.add_node(n2, 'Katharina') #insert at a leaf
    tree.level_traversal()

    tree.delete_node("Peter") #deleting a leaf
    tree.level_traversal()

    tree.delete_node("Hannah") #deleting in the middle
    tree.level_traversal()

    tree.delete_node("Origin") #deleting The root
    tree.level_traversal()
