#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Binary tree data structure in Python. Practical work n°4 INLO

@author: ghassan

Quote of the day:
    "You don't have to look away from your screen to appreciate the beauty
    of trees."
                        ~ Ghassan Dabane
                                29/01/2021

Fun fact: i have a ctrl + c detector on my scripts so feel free to copy
whatever you want xD :P
"""
from __future__ import absolute_import, division
from typing import Any, List
from math import log
import re

class TreeNode:
    """A class to represent nodes in a binary tree.

    Attributes
    ----------
    data: Any
        The data to be held in a node.
    right_child: None
        The right child.
    left_child: None
        The left child.
    """

    def __init__(self: object, data: Any) -> None:
        """Class constructor.

        Parameters
        ----------
        data : Any
            The value of the given node.

        Returns
        -------
        None
            A class instance.

        """

        self.data = data
        self.right_child = None
        self.left_child = None

    def __str__(self: object) -> str:
        """Prints out a newick-like representation of node (along all its
        successors).

        Returns
        -------
        str
            (left;right):node newick-like.

        """

        if self.is_leaf():
            return str(self.data)

        return "(%s;%s):%s" %(str(self.left_child), str(self.right_child),
                              str(self.data))

    def __repr__(self: object) -> str:
        """Coder friendly representation of the object.

        Returns
        -------
        str

        """
        return "TreeNode(%s, left: %s, right: %s)" % (self.data,
                                                      self.left_child,
                                                      self.right_child)

    def is_leaf(self: object) -> bool:
        """Checks if the node is a leaf.

        Returns
        -------
        bool

        """
        return self.right_child is None and self.left_child is None

    def is_full_node(self: object) -> bool:
        """Checks if the node is a full node i.e; has two children.


        Returns
        -------
        bool

        """
        return self.right_child is not None and self.left_child is not None

    def atleast_one_child(self: object) -> bool:
        """Checks if the node has a minimum of one successor.

        Returns
        -------
        bool

        """
        return self.right_child is not None or self.left_child is not None

class Tree:
    """A class to represent binary trees, a binary tree is a collection of
    binary nodes.

    Attributes
    ----------

    root: TreeNode
        The root of the tree.
    height: float
        The height of the tree given by log2(N) - 1 (see property for
                                                     explanations).

    """

    def __init__(self: object, root_node: TreeNode) -> None:
        """Class constructor.

        Parameters
        ----------
        root : TreeNode
            The root node of the tree.

        Returns
        -------
        None
            A class instance.

        """
        self.root = root_node

    @property
    def height(self: object) -> float:
        """The maximum number of nodes at a given level l is 2^l,
        the Maximum number of nodes in a binary tree of
        height ‘h’ is 2^(h+1) – 1, because starting level is 0
        and thus the height is 2^(h+1) - 1 = N => h = log2(N+1) - 1

        Returns
        -------
        float
            the height of the tree (number of levels starting from 0).

        """

        n_nodes = self.count_all_nodes(self.root)
        return log(n_nodes + 1, 2) - 1

    @staticmethod
    def nwk_to_postorder(nwk_str: str) -> List[str]:
        """This method constructs a post-order traversal list of a binary tree
        from a given newick-like string.

        Parameters
        ----------
        nwk_str : str
            the string to be transformed into a list.

        Returns
        -------
        List[str]
            A list of the post-order dfs traversal of a tree.
        """

        commas = re.sub('[:;]',',', nwk_str)
        no_braces = re.sub('[()]','', commas)
        return no_braces.split(',')

    @staticmethod
    def postorder_to_root(postorder: List[Any]) -> TreeNode:
        """This method transforms a post-order traversal of a list into a tree
        node recursively.

        Parameters
        ----------
        postorder : List[Any]
            The post-order traversal list of a tree (can be obtained with
                                                     previous method).

        Returns
        -------
        TreeNode
            The root node of a tree.

        """

        end = len(postorder) - 1
        mid = int(end / 2)
        root = TreeNode(postorder[end])

        if len(postorder) == 3:
            root.right_child = TreeNode(postorder[mid])
            root.left_child = TreeNode(postorder[0])
        else:
            root.left_child = Tree.postorder_to_root(postorder[:mid])
            root.right_child = Tree.postorder_to_root(postorder[mid:end])

        return root

    @classmethod
    def tree_from_nwk(cls: object, nwk: str) -> object:
        """This method constructs a binary tree from a given newick-like
        format, it first constructs the post-order sequence from the string
        then attempts to build a full binary tree from it.

        Parameters
        ----------
        nwk : str
            the newick-like string to be manipulated.

        Returns
        -------
        object
            A Tree class instance.
        """
        post_order_list = Tree.nwk_to_postorder(nwk)
        root = Tree.postorder_to_root(post_order_list)

        return cls(root)

    def level_traversal(self: object) -> None:
        """Breadth-first traversal of a tree (level order).

        Returns
        -------
        None
            Prints a bfs traversal of a tree object.

        """

        if self.root is None:
            return

        curr_level = [self.root]

        while curr_level:
            next_level = []
            for node in curr_level:
                print(node.data, end="\t")
                if node.left_child:
                    next_level.append(node.left_child)
                if node.right_child:
                    next_level.append(node.right_child)
            print(end="\n")
            curr_level = next_level

    def print_newicklike(self: object) -> str:
        """Returns a depth-first post-order representation of a tree (similar
        to newick text tree format).

        Returns
        -------
        str
            A string representation of a tree.

        """
        return str(self.root)


    def dfs_postorder(self, node: TreeNode) -> None:
        """Depth-first Postorder Traversal of a tree (Left-Right-Root).

        Parameters
        ----------
        node : TreeNode
            The root node to start from.

        Returns
        -------
        None
            prints the dfs postorder tree.

        """

        if node:
            self.dfs_postorder(node.left_child)
            self.dfs_postorder(node.right_child)
            print(node.data)

    def dfs_preorder(self: object, node: TreeNode) -> None:
        """Depth-first Preorder Traversal of a tree (Root-Left-Right).

        Parameters
        ----------
        node : TreeNode
            DESCRIPTION.

        Returns
        -------
        None
            DESCRIPTION.

        """

        if node:
            print(node.data)
            self.dfs_postorder(node.left_child)
            self.dfs_postorder(node.right_child)

    def dfs_inorder(self: object, node: TreeNode) -> None:
        """Depth-first Inorder Traversal of a tree (Left-Root-Right).

        Parameters
        ----------
        node : TreeNode
            The root node.

        Returns
        -------
        None
            prints the dfs inorder tree.

        """

        if node:
            self.dfs_postorder(node.left_child)
            print(node.data)
            self.dfs_postorder(node.right_child)

    def count_leafs(self, node: TreeNode) -> int:
        """This method counts the number of leafs in the given binary tree.

        Parameters
        ----------
        node : TreeNode
            The starting node (or the root node).

        Returns
        -------
        int
            The number of leafs.

        """

        if node is None:
            return 0

        if node.is_leaf():
            return 1

        return self.count_leafs(node.left_child) + self.count_leafs(
            node.right_child)

    def count_full_nodes(self: object) -> int:
        """Counts the number of full nodes in the given binary tree.

        Returns
        -------
        int
            The number of full nodes.

        """

        if self.root is None:
            return 0

        count = 0
        queue = [self.root]

        while len(queue) > 0:

            node = queue.pop(0)

            if node.is_full_node():
                count += 1

            if node.left_child is not None:
                queue.append(node.left_child)

            if node.right_child is not None:
                queue.append(node.right_child)

        return count

    def count_all_nodes(self, node: TreeNode) -> int:
        """A recursive method to count the number of nodes given in a tree.

        Parameters
        ----------
        node : TreeNode
            The starting node (the root node).

        Returns
        -------
        int
            The number of all nodes that are present in the tree.

        """

        if node is None:
            return 0

        return 1 + self.count_all_nodes(
            node.left_child) + self.count_all_nodes(node.right_child)

    def add_node(self: object, added_node: TreeNode, target_data: Any) -> None:
        """This method adds a node to a tree at a given position (after a
        precise node).

        Parameters
        ----------
        added_node : TreeNode
            The new node to be added.
        target_data : Any
            the data of the node to be searched in a tree.

        Returns
        -------
        None

        """

        if self.root is None:
            self.root = added_node

        queue = [self.root]

        while len(queue) > 0:

            node = queue.pop(0)

            if node.data == target_data:
                if node.left_child is None:
                    node.left_child = added_node
                else:
                    node.right_child = added_node
                break

            if node.left_child:
                queue.append(node.left_child)
            if node.right_child:
                queue.append(node.right_child)

    def delete_node(self: object, target_data: Any) -> None:
        """This method deletes a targeted node, if it's a leaf it erases it
        completely, otherwise, it deletes it and puts the right child in its
        place (or the left child if the right child is empty).

        Parameters
        ----------
        target_data : Any
            The value of the node to be deleted.

        Returns
        -------
        None

        """

        if self.root is None:
            return

        if self.root.is_leaf():
            if self.root.data == target_data:
                self.delete_leaf(self.root)

        queue = [self.root]

        while len(queue) > 0:

            node = queue.pop(0)

            if node.data == target_data:

                if node.is_leaf():
                    self.delete_leaf(node)
                elif node.right_child is not None:
                    node.data = node.right_child.data
                    node.right_child = None
                else:
                    node.data = node.left_child.data
                    node.left_child = None

            if node.left_child:
                queue.append(node.left_child)
            if node.right_child:
                queue.append(node.right_child)

    def delete_leaf(self: object, leaf: TreeNode) -> None:
        """This method deletes a given leaf in a tree. it iteratively passes
        on nodes to find the parent of the searched node, then it deletes
        its child (the node that we want to delete).

        Parameters
        ----------
        leaf : TreeNode
            The node to be deleted.

        Returns
        -------
        None

        """

        queue = [self.root]
        while len(queue) > 0:
            temp = queue.pop(0)
            if temp is leaf:
                temp = None
                return
            if temp.right_child:
                if temp.right_child is leaf:
                    temp.right_child = None
                    return
                queue.append(temp.right_child)
            if temp.left_child:
                if temp.left_child is leaf:
                    temp.left_child = None
                    return
                queue.append(temp.left_child)
