#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
A script to calculate genomic length for each transcript in a gtf file.
"""
from __future__ import absolute_import
import re

transcriptStart = dict()
transcriptEnd = dict()


def get_genomic_tx_length(input_file:str =None) -> None:
    """This function computes the genomic length for each transcript
    from a given/specified .gtf file.

    Parameters
    ----------
    input_file: str
        .gtf file path

    Returns
    -------
    None.
    """
    file_handler = open(input_file)
    for line in file_handler:
        token = line.split("\t")
        start = int(token[3])# le début de l'élément courant
        end = int(token[4])# la fin de l'élément courant
        # L'identifiant du transcrit

        tx_id = re.search('transcript_id "([^"]+)"', token[8]).group(1)

        if tx_id not in transcriptStart:

            transcriptStart[tx_id] = start
            transcriptEnd[tx_id] = end

        else:

            if start < transcriptStart[tx_id]:

                transcriptStart[tx_id] = start

                if end > transcriptEnd[tx_id]:
                    transcriptEnd[tx_id] = end

    for tx_id in transcriptStart:
        print(tx_id + "\t" + str(transcriptEnd[tx_id] - transcriptStart[tx_id] + 1))


if __name__ == '__main__':
    get_genomic_tx_length(input_file='../pymetacline/data/gtf/simple.gtf')
