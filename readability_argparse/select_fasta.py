"""
A CLI to select certain sequences from a given fasta file.
"""
from __future__ import absolute_import
import argparse
import sys
from typing import List
from Bio import SeqIO

def selec_seqs_list(fasta: str, ids: List[str]) -> None:
    """
    This function prints chosen sequences of ids from a given .fa file.

    Parameters
    ----------
    fasta : str
        fasta file path.
    ids : List[str]
        A list of fasta IDs to be searched in the given fasta file.

    Returns
    -------
    None
        Prints only the ids with their sequences.

    """

    for record in SeqIO.parse(fasta, 'fasta'):
        if record.name in ids:
            print(">%s \n%s"%(record.name, record.seq))


def create_parser() -> argparse.ArgumentParser:
    """This function creates the parser for our CLI."""

    parser = argparse.ArgumentParser(add_help=True,
                                     description="""Select sequences from a fasta file-
                                     Please install Biopython before using""")
    parser.add_argument('-i','--input_file',
                        type=argparse.FileType('r'),
                        default=sys.stdin,
                        help="[REQUIRED] The fasta file path")
    parser.add_argument('-seq','--seq_ids',
                        type=str,
                        required=True,
                        help="[REQUIRED] A comme separated list of sequence IDs")
    return parser

def main() -> None:
    """This function instantiates an argument parser object and calls
    selec_seqs_list function to return sequences for user specified IDs
    from the given fasta file.
    """
    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__
    entered_ids = args['seq_ids'].split(',')
    selec_seqs_list(args['input_file'], entered_ids)


if __name__ == "__main__":
    main()
