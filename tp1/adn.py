"""
Author : Ghassan DABANE

Script Python dans le cadre de l'UE INLO de M1 Bioinformatique, TP 
d'apprentissage à l'utilisation de git.
"""
import re

def is_valid(adn: str) -> bool : 
    
    #all(nuc in ['A','T','C','G'] for nuc in adn) old slow method ;(
    return bool(re.compile('^[ATCG]+$').match(adn))

def get_valid_adn() -> None : 

    while not is_valid(input("Entrez une chaîne d'ADN \n \n")) : 
        print("La chaîne d'ADN n'est pas valide")

    print("La chaîne d'ADN est valide")
